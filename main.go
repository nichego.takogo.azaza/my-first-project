package main

import ( //Для решения проблем взял из инета такие библиотеки как: bufio, log, os, strconv
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func Rm(s int) (Aw string) { //Создаем римское число из арабского для выдачи ответа
	for i := 1; i <= 3; i++ {
		if (s-i)%5 == 0 {
			if (s-i)%10 != 0 {
				Aw = Aw + "V"
			}
			for ; i != 0; i-- {
				Aw = Aw + "I"
			}
			i = 4
		}
	}
	for i := 0; i <= 1; i++ {
		if (s+i)%5 == 0 {
			if i == 1 {
				Aw = Aw + "I"
			}
			if (s+i)%10 == 0 {
				if i == 1 {
					Aw = Aw + "X"
				}
			} else {
				Aw = Aw + "V"
			}
			i = 4
		}
	}
	s = s / 10
	if s == 10 {
		Aw = "C"
	} else if s == 5 {
		Aw = "L" + Aw
	} else if s == 9 {
		Aw = "XC" + Aw
	} else if s == 4 {
		Aw = "XL" + Aw
	} else {
		if s > 5 {
			s -= 5
			R = false
		}
		for ; s != 0; s-- {
			Aw = "X" + Aw
		}
		if !R {
			Aw = "L" + Aw
		}
	}
	fmt.Println(Aw)
	return
}

func An() {
	fmt.Println("Ответ: ")
}
func fPlus(a, b int) { //Функция сложения
	An()
	if R {
		Rm(a + b)
	} else {
		fmt.Println(a + b)
	}
}
func fMinus(a, b int) { //Функция вычитания
	An()
	if R {
		if (a - b) <= 0 {
			fmt.Println("ERROR! Ответом не может являться римское число меньше еденицы")
			log.Fatal(a - b)
		}
		Rm(a - b)
	} else {
		fmt.Println(a - b)
	}
}
func fMultiply(a, b int) { //Функция перемножения
	An()
	if R {
		Rm(a * b)
	} else {
		fmt.Println(a * b)
	}
}
func fDivide(a, b int) { //Функция деления
	An()
	if R {
		Rm(a / b)
	} else {
		fmt.Println(a / b)
	}
}

var R = false

func main() {
	r0 := 0
	r2 := 0
	text := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите арифметическое выражение с использованием пробелов, не более двух операндов, каждый из которых должен попадать в диапозон от 1 до 10 включительно, можно использовать как арабские так и римские цифры, но нельзя их смешивать, доступные операторы: + - * /")
	text.Scan()
	f := (strings.Split(text.Text(), " "))
	if len(f) == 3 { //Проверка на количество пробелов и строк разделенных этими пробелами
		for i := 0; i < 3; i = i + 2 { //Проверка на римские цифры
			h := 0
			switch f[i] {
			case "I":
				h = 1
			case "II":
				h = 2
			case "III":
				h = 3
			case "IV":
				h = 4
			case "V":
				h = 5
			case "VI":
				h = 6
			case "VII":
				h = 7
			case "VIII":
				h = 8
			case "IX":
				h = 9
			case "X":
				h = 10
			default:
				if i == 0 {
					i = 3
				} else if i == 2 {
					fmt.Println("ERROR! Нельзя смешивать арабские и римские цифры")
					log.Fatal(f[i])
				}
			}
			if i == 0 {
				r0 = h
			} else if i == 2 {
				r2 = h
				R = true //Если обе цифры римские и входят в диапазон
			}
		}
		f0, err := strconv.Atoi(f[0]) //Проверка на правильность ввода чисел
		if err != nil && !R {
			fmt.Println("ERROR! Вы написали непонятный программе символ")
			log.Fatal(err)
		}
		f2, err := strconv.Atoi(f[2])
		if err != nil && !R {
			fmt.Println("ERROR! Вы написали непонятный программе символ")
			log.Fatal(err)
		}
		if R {
			f0 = r0
			f2 = r2
		}
		if f0 >= 1 && f0 <= 10 && f2 >= 1 && f2 <= 10 { //Проверка входят ли числа в диапазон
			switch f[1] { //Проверка оператора
			case "+":
				fPlus(f0, f2)
			case "-":
				fMinus(f0, f2)
			case "*":
				fMultiply(f0, f2)
			case "/":
				fDivide(f0, f2)
			default:
				fmt.Println("Error! Программа не поняла что нужно сделать с числами, программа понимает только: +, -, *, /")
			}
		} else {
			fmt.Println("ERROR! Число выходит из диапозона от 1 до 10 включительно")
		}
	} else {
		fmt.Println("ERROR! Данные введены некорректно, неправильное разделение пробелами")
	}
}
